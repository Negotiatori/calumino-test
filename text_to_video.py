import numpy as np
from numpy import uint8
import cv2
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler

def main():

    fourcc = cv2.VideoWriter_fourcc(*'MJPG')
    out = cv2.VideoWriter('thermal.avi', fourcc, 10, (35,17))

    with open('raw_GateCounting_1_CTS000850_201006172744_0.txt', 'r') as file:
        data = file.readlines()

    if data[0] < data[len(data) - 1]:
        data = data[2:]

    with open('thermal_video.txt', 'w') as file:
        file.writelines( data )

    df = pd.read_table('thermal_video.txt', delimiter="\t", header=None)

    df = df.iloc[:,0:595]
    print(df.shape)

    cmap = plt.cm.jet
    norm = plt.Normalize(vmin=20, vmax=32)

    for row in range(0,32):
        pixel_row = 0
        pixel_col = 0
        temp_img = pd.DataFrame(index=range(17),columns=range(34))
        for col in range(0,df.shape[1]):
            if pixel_col < 34:
                temp_img.iloc[[pixel_row],[pixel_col]] = df.iloc[[row],[col]]
                temp_img.iloc[[pixel_row],[pixel_col]] = temp_img.iloc[[pixel_row],[pixel_col]].astype(float)
                pixel_col += 1
            else:
                pixel_col = 0
                pixel_row += 1

        temp_img = temp_img.replace(to_replace=np.nan, value='23.848')
        temp_img = temp_img.astype(float)

        np_image = np.array(temp_img, dtype=uint8)

        scaler = MinMaxScaler(feature_range=(0,255))
        model = scaler.fit(np_image)
        image = model.transform(np_image)

        image = image.astype(uint8)
        image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)

        out.write(image)

    out.release()

    
    cv2.imwrite("test_img.jpg", image)
    print("done")
    

if __name__ == "__main__":
    main()