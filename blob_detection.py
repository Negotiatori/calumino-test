import cv2
import numpy as np

def main():

    fourcc = cv2.VideoWriter_fourcc(*'MJPG')
    out = cv2.VideoWriter('blob_detection.avi', fourcc, 10, (204,102))

    cap = cv2.VideoCapture('thermal.avi')

    if (cap.isOpened()== False): 
        print("Error")

    while(cap.isOpened()):
        ret,img = cap.read()
        if ret == False:
            break    
        
        # Scaling image to make blobs easer to view
        scale_percent = 600
        width = int(img.shape[1] * scale_percent / 100)
        height = int(img.shape[0] * scale_percent / 100)
        dim = (width, height)
        img_resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
        img_resized = cv2.bitwise_not(img_resized)

        params = cv2.SimpleBlobDetector_Params()
        # Change thresholds
        params.minThreshold = 10
        params.maxThreshold = 200

        # Filter by area
        params.filterByArea = True
        params.minArea = 450

        # Filter by circularity
        params.filterByCircularity = True
        params.minCircularity = 0.1

        # Filter by Convexity
        params.filterByConvexity = True
        params.minConvexity = 0.7

        # Filter by Inertia
        params.filterByInertia = True
        params.minInertiaRatio = 0.01


        ver = (cv2.__version__).split('.')
        if int(ver[0]) < 3 :
            detector = cv2.SimpleBlobDetector(params)
        else : 
            detector = cv2.SimpleBlobDetector_create(params)

        detector.empty() 
        keypoints = detector.detect(img_resized)

        # Counting number of blobs
        people = 0
        for point in keypoints:
            people += 1
        print_str = "people: " + str(people)

        img_resized = cv2.putText(img_resized, print_str, (20,20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 100, 0), 1, cv2.LINE_AA)
        im_with_keypoints = cv2.drawKeypoints(img_resized, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

        cv2.imshow("frame", im_with_keypoints)

        if cv2.waitKey(25) & 0xFF == ord('q'):
            break

        out.write(im_with_keypoints)

    out.release()
    cap.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    main()